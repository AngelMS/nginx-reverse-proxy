# FROM nginx:stable
FROM nginx:1.24.0

COPY nginx.conf /etc/nginx/nginx.conf
COPY node-cron-renewer node-cron-renewer
COPY wrapper.sh wrapper.sh

#########
# Note that apt-get update and apt-get install are executed in a single RUN instruction. 
# This is done to make sure that the latest packages will be installed. 
# If apt-get install were in a separate RUN instruction, then it would reuse a layer added by apt-get update, 
# which could had been created a long time ago.
#########

# The recommended way to install cerbot is snap (not a great thing with docker) and the auto scrip has been deprecated
# There are two alternatives: 
# 			- installing using pip https://certbot.eff.org/instructions?ws=nginx&os=pip
# 			- install the latest version distributed with the OS
# We are using the ubuntu version for simplicity. I just tested this one, it works and it's easier to mantain (just a simple apt install)
# Ubuntu instructions found here: https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/#1.-Download-the-Let%E2%80%99s&nbsp;Encrypt-Client
RUN apt update && apt install -y \
certbot python3-certbot-nginx nodejs

CMD ./wrapper.sh