const subProcess = require('child_process')

let cron = require('node-cron');


// “At 03:00 on day-of-month 5.”
cron.schedule('0 3 5 * *', () => {
  console.log(`> Renewing certificates... [${new Date().toUTCString()}]`);

  subProcess.exec('certbot renew', (err, stdout, stderr) => {
    if (err) {
      console.error(`> node-cron-renewer Error: ${err}`)
      process.exit(1)
    } else {
      console.log(`stdout: ${stdout.toString()}`)
      console.log(`stderr: ${stderr.toString()}`)
    }
  })

});