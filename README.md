# Nginx Docker Reverse Proxy

Auto contained deployment of [Nginx](https://www.nginx.com/) + [Certbot](https://certbot.eff.org/) ([Let's Encrypt](https://letsencrypt.org/)) - ready to serve & certify 

### Configure Nginx

After pulling the project, configure all the websites that Nginx will serve (Only HTTP, certbot will take care of the HTTPS configurations)

```
conf.d/default.conf
```



```
# Change "example.com" to your custom domain name
server_name  example.com;


# Change "http://xxx.xxx.xxx.xxx:yyyy/" to the http://ip:port of the local network machine with the web server
proxy_pass http://xxx.xxx.xxx.xxx:yyyy/;
```


### Launch Nginx
```
docker-compose up -d
```


### Certbot: Get a new certificate (1st time only)

Get into the runing container

```
sudo docker exec -it reverse-proxy bash
```

Launch the autoconfiguration process

```
certbot --nginx
```

After following the propmted options your website will be served as https:// and your certificates will be located at

```
./letsencrypt/live/yourwebsite/
```





### Auto Renew certificates

The container now manages auto renewal itself using a minimal node-cron instance. (Docker does not work well with native crontab)

The default renewal period is "0 3 5 * *" (At 03:00 on day-of-month 5) to change it edit **node-cron-renewer/index.js**

You can use [https://crontab.guru](https://crontab.guru/#0_5_1_*_*) to write custom crontab configurations effortlessly
