#!/bin/bash

# Official docs
# https://docs.docker.com/config/containers/multi-service_container/#use-a-wrapper-script

# Start the first process
node node-cron-renewer/index.js &

# Start the second process
nginx -g 'daemon off;' &

# Wait for any process to exit
wait -n

# Exit with status of process that exited first
exit $?